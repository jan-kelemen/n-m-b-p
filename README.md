# NMBP - Napredni modeli i baze podataka (Advanced Databases)
This folder contains solutions to laboratory exercises of Advanced Databases course.

## Content
* P1 - Text search and advanced SQL
* P2 - GIS
* P3 - NoSQL and MapReduce

Unfinished project (P4 - Semantic web) is on it's separate *p4* branch.
